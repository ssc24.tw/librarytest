FB看到的JAVA問題
原始內容如下:
=======================================================
之前面試被問到兩個問題，一直不知道有什麼好的方式回答(面試官是預設使用Java)

問題一: 假設目前有一個API, getID(int id) ，你想要換成新版的api, ex: getID(String id) 不維護舊的。但目前有很多client 都在用舊的API，你要如何處理?

=> 這邊本人弱弱的回答是用是包不同版本的Library。新版本的Library 就把舊的API標記depreciated。如果client要用因為其他需求要用新的就只能用新版的api

問題二: 假設有Library A 用到Library C 1.0 版本，而Library B 用到 Library C 2.0 版本。你的程式需要用到Library A和 Library B，但Library C 1.0和2.0不相容，你會如何處理?

=>我的回答是用不同的class loader 載入不同的Library，但這邊因為我回答不出class loader的細節所以就一直被打槍。

想請問版上高手有沒有比較好的解法?


LibraryA call libraryC:
I am Library Cv1.0
LibraryB call libraryC:
I am Library Cv2.0