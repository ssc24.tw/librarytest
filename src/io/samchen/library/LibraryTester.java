package io.samchen.library;

public class LibraryTester {

	public static void main(String[] args) {
		LibraryA la = new LibraryA();
		la.doMyWork();
		
		LibraryB lb = new LibraryB();
		lb.doMyWork();
	}

}
