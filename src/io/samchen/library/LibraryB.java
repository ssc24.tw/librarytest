package io.samchen.library;

public class LibraryB {
	private LibraryC libraryC;
	
	public void doMyWork() {
		libraryC = new LibraryCv20();
		System.out.println("LibraryB call libraryC:");
		libraryC.doStart();
	} 
}
