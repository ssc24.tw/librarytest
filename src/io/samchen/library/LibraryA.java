package io.samchen.library;

public class LibraryA {
	private LibraryC libraryC;
	
	public void doMyWork() {
		libraryC = new LibraryCv10();
		System.out.println("LibraryA call libraryC:");
		libraryC.doStart();
	} 
}
