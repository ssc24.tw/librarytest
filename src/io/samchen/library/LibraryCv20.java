package io.samchen.library;

public class LibraryCv20 implements LibraryC {
	private final String VERSION = "v2.0";
	@Override
	public String getVersion() {
		return VERSION;
	}
	@Override
	public void doStart() {
		System.out.println("I am Library C" + this.getVersion());
	}

}
